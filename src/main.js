// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './App'

import Home from './components/Home'
import Result from './components/SearchResult'
// import News from './components/myNews'
import Detail from './components/DetalleArt'
import Panel from './components/PanelUser'

const routes = [
  { path: '/home', component: Home, alias: '/' },
  { path: '/result', component: Result },
  { path: '/detail', component: Detail },
  // { path: '/myNews', component: News },
  { path: '/panel', component: Panel }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
